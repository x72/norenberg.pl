const ClutteredSpace = (hp) => ({
  isClutteredSpace: true,
  name: "Cluttered space",
  occupants: [],
  tip: (m) => html`<div>${m.hp}/${m.hpMax}</div>`,
  hp: hp || 15,
  hpMax: hp || 15,
  work: (ch, m) => {
    G.inv.scrap += 5;
    m.hp -= 5;
  },
  afterwork: (ch, m) => {
    if (m.hp <= 0) {
      var idx = G.modules.indexOf(m);
      G.modules[idx] = EmptySpace();
    }
  },
  moduleTip: (ch, m) => html`<div>unclutter to gain 5 scrap</div>`,
  moduleBonusTip: (ch) => "",
});

const EmptySpace = () => ({
  isEmpty: true,
  name: "Empty space",
  occupants: [],
  tip: "",
  work: () => false,
  moduleTip: (ch) => "ready to build something here (use build menu)",
  moduleBonusTip: (ch) => "",
});

var G = {
  cheats_used: 0,
  alert: "",
  unskilledCopy: (ch) => `${ch.n}: unskilled`,
  travel: 1,
  confirm_cancel: () => { G.confirm = ""; refresh(); },
  mode: "travel",
  explorationInfo: [],
  fail: false,
  inv: {
    food: 4,
    water: 10,
    scrap: 40,
    veggies: 10,
    ammo: 20,
    meat: 0,
    firstAidKits: 1,
    wool: 10,
    alcohol: 4,
  },
  curChar: null,
  chOver: null,
  chars: [
    { exp:3, expT:3, n:"Anna", leader:true, img: "leader_minature_anna.png", a:1, amax:1, hp:1,
      skillTip: (ch) => `Shooting (level ${ch.skillShooting}) - deals higher damage when shooting`,
      info: "Shooter",
      skillShooting: 1,
    },
    { exp:5, expT:4, n:"Ethan", leader:true, img: "leader_minature_ethan.png", a:1, amax:1, hp:1,
      skillTip: (ch) => `Healing (level ${ch.skillHeal}) - heals allies much faster and more effectively`,
      info: "Medic",
      skillHeal: 0,
    },
    { exp:2, expT:2, n:"Melvit", leader:true, img: "leader_minature_melvit.png", a:1, amax:1, hp:1,
      skillTip: (ch) => `Toughness (level ${ch.skillToughness}) - is more difficult to take down`,
      info: "Tank",
      skillToughness: 1,
    },
    { exp:1, expT:3, n:"Jo", leader:false, img: "worker_female_2.png", a:2, amax:1, hp:1.0,
      skillTip: (ch) => `Gardening (level ${ch.skillGardening}) - gathers more veggies`,
      info: "Gardener",
      skillGardening: 2,
    },
    { exp:7, expT:5, n:"Kajetan", leader:false, img: "worker_male_7.png", a:2, amax:1, hp:1.0,
      skillTip: (ch) => `Medical Training (level ${ch.skillMed}) - is better at producing first-aid kits`,
      info: "Laborant",
      skillMed: 0,
    },
    { exp:2, expT:3, n:"Sigfrid", leader:false, img: "worker_male_4.png", a:2, amax:1, hp:1.0,
      skillTip: (ch) => `Gunsmith (level ${ch.skillAmmo}) - is better at producing ammo`,
      info: "Gunsmith",
      skillAmmo: 1,
      isLast: true, // for help only
    },
  ],
  modules: [
    ClutteredSpace(20),
    ClutteredSpace(5),
    EmptySpace(),
    EmptySpace(),
    ClutteredSpace(25),
    EmptySpace(),
    ClutteredSpace(),
  ],

  to_build: [
    {
      name: "Archive",
      tip: "Teach your crew useful skills",
      cost: 30,
      icon: html`&#128214`,
      fn: () => ({
        icon: html`&#128214`,
        name: "Archive",
        occupants: [],
        tip: "learn skills",
        work: (ch) => {
          ch.exp += 4;

          if (ch.exp >= 10) {
            ch.exp %= 10;
            // each crewmember has only one anyway!
            try { ch.skillShooting++; } catch(e) {};
            try { ch.skillHeal++; } catch(e) {};
            try { ch.skillToughness++; } catch(e) {};
            try { ch.skillGardening++; } catch(e) {};
            try { ch.skillMed++; } catch(e) {};
            try { ch.skillAmmo++; } catch(e) {};
          }
        },
        moduleTip: (ch) => {
          let c = "Learn...";
          if (ch.hasOwnProperty("skillShooting")) c = "Learn shooting...";
          if (ch.hasOwnProperty("skillHeal")) c = "Learn healing...";
          if (ch.hasOwnProperty("skillToughness")) c = "Learn toughness...";
          if (ch.hasOwnProperty("skillGardening")) c = "Learn gardening...";
          if (ch.hasOwnProperty("skillMed")) c = "Learn farmacy...";
          if (ch.hasOwnProperty("skillAmmo")) c = "Learn gunsmithing...";
          return ch.n + ": " + c;
        },
        moduleBonusTip: () => "",
      }),
    },

    {
      name: "Garden",
      tip: "Gather veggies here",
      cost: 20,
      icon: html`&#129365`,
      fn: () => ({
        icon: html`&#129365`,
        name: "Garden",
        occupants: [],
        tip: "gather veggies",
        moduleTip: (ch) => "gather 5 veggies using 2 water",
        moduleBonusTip: (ch) => (
          (ch.skillGardening && ch.skillGardening > 0)
            ? `(skill bonus: +${ch.skillGardening} veggies)`
            : G.unskilledCopy(ch)
        ),
        work: (ch) => {
          if (G.inv.water < 2) {
            alert("Not enough water");
            return false;
          }
          G.inv.veggies += 5 + (ch.skillGardening || 0);
          G.inv.water -= 2;
          if (ch.hasOwnProperty("skillGardening")) { ch.exp++ };
        }
      }),
    },

    {
      name: "Kitchen",
      tip: "Turn veggies/meat into food",
      cost: 20,
      icon: html`&#127858`,
      fn: () => ({
        icon: html`&#127858`,
        name: "Kitchen",
        occupants: [],
        tip: "prepare food",
        moduleTip: (ch) => "use veggies (1) or meat (1) to prepare food (2)",
        moduleBonusTip: (ch) => G.unskilledCopy(ch),
        work: (ch) => {
          if (G.inv.veggies == 0 && G.inv.meat == 0) {
            alert("We need veggies or meat to produce food!");
            refresh();
            return false;
          }

          while(true) {
            if (Math.random() > 0.5) {
              if (G.inv.veggies > 0) { G.inv.veggies--; break; }
            } else {
              if (G.inv.meat > 0) { G.inv.meat--; break; }
            }
          }

          G.inv.food += 2; // Noone has cooking skills!
        },
      }),
    },
    {
      name: "Gunsmith",
      tip: "Produce ammo",
      cost: 30,
      icon: html`&#128299`,
      fn: () => ({
        icon: html`&#128299`,
        name: "Gunsmith",
        occupants: [],
        tip: "produce ammo",
        moduleTip: (ch) => `produce 10 ammo for 5 scrap`,
        moduleBonusTip: (ch) => (
          (ch.skillAmmo && ch.skillAmmo> 0)
            ? `(skill bonus: +${ch.skillAmmo} ammo)`
            : G.unskilledCopy(ch)
        ),
        work: (ch) => {
          if (G.inv.scrap < 5) {
            alert("I need some scrap");
            return false;
          }

          G.inv.scrap -= 5;
          G.inv.ammo += 10 + (ch.skillAmmo || 0);
          if (ch.hasOwnProperty("skillAmmo")) { ch.exp++ };
        },
      }),
    },
    {
      name: "Bar",
      tip: "Make crew forget how unhappy they are",
      cost: 20,
      icon: html`&#127866`,
      fn: () => ({
        icon: html`&#127866`,
        name: "Bar",
        occupants: [],
        isBar: true,
        tip: "cure discontent",
        work: (ch) => {
          if (G.inv.alcohol <= 0) {
            alert("Nothing to drink :(");
            return false;
          }
          G.inv.alcohol--;
          ch.unhappy = false;
        },
        moduleTip: (ch) => `makes crewmember forget they are unhappy for a while (consumes 1 alcohol)`,
        moduleBonusTip: (ch) => "",
      }),
    },
    {
      name: "Hospital",
      tip: "Heal your crew",
      icon: html`&#127973`,
      cost: 40,
      fn: () => ({
        icon: html`&#127973`,
        name: "Hospital",
        occupants: [],
        tip: "heal crew",
        work: (ch) => {
          if (ch.hp >= 1.0) { alert("I don't need healing");
            return false;
          }
          ch.hp = Math.min(1.0, ch.hp + 0.4);
        },
        moduleTip: (ch) => `heal crewmember of 40 dmg`,
        moduleBonusTip: (ch) => "",
      }),
    },
    {
      name: "Lab",
      tip: "Produce first-aid kits",
      cost: 35,
      icon: html`&#129515`,
      fn: () => ({
        icon: html`&#129515`,
        name: "Lab",
        occupants: [],
        tip: "produce first-aid kits",
        work: (ch) => {
          if (G.inv.wool < 2 || G.inv.alcohol < 1) {
            alert("Missing ingredients");
            return false;
          }
          G.inv.wool -= 1;
          G.inv.alcohol -= 1;
          G.inv.firstAidKits += 1 + (ch.skillMed || 0);
          if (ch.hasOwnProperty("skillMed")) { ch.exp++ };
        },
        moduleTip: (ch) => `produce first-aid kit out of wool (1) and alcohol (1)`, // ??
        moduleBonusTip: (ch) => (
          (ch.skillMed && ch.skillMed> 0)
            ? `(skill bonus: +${ch.skillMed} first-aid kit)`
            : G.unskilledCopy(ch)
        ),
      }),
    }
  ]
}
G.curChar = G.chars[0];

const alert = (txt) => G.alert = txt;
const confirm = (txt) => G.confirm = txt;

const Hp = (ch) => {
  return html`<div class="hp"><div style="width: ${ch.hp * 100}%"></div></div>`;
}

const Portrait = (ch) => {
  return html`
    <li
      class="portrait ${Help.fc(ch.leader ? "portrait-l" : "portrait-w")} ${ch.isDead ? "dead" : ""} ${ch.a == 0 ? "off" : ""} ${G.curChar == ch ? 'portrait-sel' : '' } ${ch.leader ? "l" : "w"}"
      @click="${() => { G.curChar = ch; G.toBeBuilt = null; refresh(); } }"
      @mouseover="${() => { G.curTip = ch; refresh(); }}"
      @mouseout="${() => { G.curTip = (G.chOver == ch) ? null : G.chOver; refresh(); }}"
    >
      <span class="potrait-is-dead-label">${ch.isDeserter ? "DESERTED" : "DEAD"}</span>
      <img src="${ch.img}" alt="">
      <span class="portrait-name">
        ${ch.n} <br> <small style="font-weight: normal; opacity: 0.7;">${ch.info}</small>
        <span class="portrait-sad ${ch.unhappy ? "" : "hid"}">😟</span>
      </span>
      ${Hp(ch)}
      <div class="${Help.fc("p-ap")}">
        <div class="act ${ch.a>=1 ? 'act-on' : ''}"></div>
        <div class="act ${ch.a>=2 ? 'act-on' : ''} ${ch.leader ? 'hid' : ''}"></div>
      </div>

      <div class="
        tip
        tip-portrait
        ${ch.isLast ? Help.fc("p-hover") : ""}
        ${((ch == G.curTip) || (ch.isLast && Help.fc("p-hover"))) ? "" : "hid"}"
      >
        <b>${ch.n}</b>
        <br>
        <span class="c">Learning:</span>
          ${
            Array.from('x'.repeat(10)).map((_, i) => {
              return i < ch.exp ? html`&#9679;` : html`&#9675;`
            })
          }
        <br>
        <span class="c">Skill:</span> ${ch.skillTip(ch)}
        <br>
        <div class="r ${ch.unhappy ?"":"hid"}">Discontent: hungry and thirsty crewmember will leave you soon!</div>
      </div>
    </li>
  `;
}

const Portraits = (chars) => html`
  <ul class="portraits ${Help.fc("portraits")}">
    ${chars.map((ch) => Portrait(ch))}
  </ul>
`;

const Modules = () => {
  return html`
    <ul class="ms ${Help.fc("modules")}">
      ${Module(0)}
      ${Module(1)}
      ${Module(2)}
      ${Module(3)}
      ${Module(4)}
      ${Module(5)}
      ${Module(6)}
    </ul>
  `;
}

const doWork = (ch, m) => {
  if (m.occupants.length >= 2 && !m.occupants.includes(ch))
  {
    alert("Too many people in this place already!");
    refresh();
    return;
  }

  if (ch && ch.a > 0) {
    var ok = m.work(ch, m);

    if (ok !== false) {
      ch.a--;

      G.modules.forEach(mm => {
        mm.occupants = mm.occupants.filter(other => other != ch);
      });

      m.occupants.push(ch);

      if (m.afterwork) m.afterwork(ch, m);
    }

  } else if (ch && ch.a == 0) {
    alert("I've already used up all my action points!");
  } else {
    alert("Cannot do it"); // ??
  }
  refresh();
}

const safeGet = (it, a, b, c) => {
  if (typeof(it) == "function") {
    return it(a, b, c);
  } else {
    return it;
  }
}

const Module = (idx) => {
  const m = G.modules[idx]


  const canAct = (!G.curChar.isDead) && G.curChar.a > 0;

  let tip = canAct
    ? html`
      <span class="m-charTip">${m.moduleTip(G.curChar, m)}</span>
      <b class="m-charTip">${m.moduleBonusTip(G.curChar, m)}</b>
    `
    : html`
      <span class="m-charTip"><i style="opacity: 0.7">no action points</i></span>
    `;

  if (G.toBeBuilt) {
    if (m.isEmpty) {
      tip = html`
        <b class="m-charTip">Build ${G.toBeBuilt.name}</b>
      `;
    } else {
      tip = html`
        <b class="m-charTip">Cannot build here</b>
      `;
    }
  }

  return html`
    <li
      class="m
        ${m.isClutteredSpace ? ("m-cluttered" + Help.fc("mod-cl")) : ""}
        ${m.isEmpty ? ("m-empty" + Help.fc("mod-e")) : ""}"
      @click=${() => G.toBeBuilt ? tryBuild(idx) : doWork(G.curChar, m)}
    >
      <strong><span class="m-icon">${m.icon || ""}</span> ${m.name}</strong>
      <small>${safeGet(m.tip, m)}&nbsp;</small>

      <br>
      <br>

      ${tip}

      <div class="occupants">
        ${m.occupants.map((c) => html`<div><span>${c.n}</span><img src=${c.img} alt=${c.n}></div>`)}
      </div>
    </li>
  `;
}

const Inventory = (G) => {
  var chs = G.chars.filter(c => !c.isDead).length

  return html`
    <div class="inv ${Help.fc("res")}">
      <span class="${Help.fc("res-fw")} ${G.inv.food < chs ? "inv-low" : ""}"><small>food:</small> ${G.inv.food}</span>
      <span class="${Help.fc("res-fw")} ${G.inv.water < chs ? "inv-low" : ""}"><small>water:</small> ${G.inv.water}</span>
      <span class="${Help.fc("res-s")}"><small>scrap:</small> ${G.inv.scrap}</span>
      <span class="${Help.fc("res-vm")}"><small>veggies:</small> ${G.inv.veggies}</span>
      <span class="${Help.fc("res-vm")}"><small>meat:</small> ${G.inv.meat}</span>
      <span class="${Help.fc("res-w")}"><small>wool:</small> ${G.inv.wool}</span>
      <span class="${Help.fc("res-a")}"><small>alcohol:</small> ${G.inv.alcohol}</span>
      <span class="${Help.fc("res-fight")}"><small>ammo:</small> ${G.inv.ammo}</span>
      <span class="${Help.fc("res-fight")}"><small>first-aid kits:</small> ${G.inv.firstAidKits}</span>
    </div>
  `;
}


const exploreEffects = [
  // () => {
  //   const opts = [
  //     () => { G.inv.water += 4; return "we found some drinkable watter (+4 water)"; },
  //     () => { G.inv.water += 2; return "we found some scrap (+2 water)"; },
  //     () => { G.inv.water += 1; return "we found some scrap (+1 water)"; },
  //   ];
  //   return opts[Math.floor(Math.random() * opts.length)]();
  // },
  () => {
    const opts = [
      () => { G.inv.water += 8; return "we found some drinkable watter (+8 water)"; },
      () => { G.inv.water += 7; return "we found some drinkable watter (+7 water)"; },
      () => { G.inv.water += 6; return "we found some drinkable watter (+6 water)"; },
      () => { G.inv.water += 5; return "we found some drinkable watter (+5 water)"; },
      () => { G.inv.water += 3; return "we found some drinkable watter (+3 water)"; },
      () => { G.inv.veggies += 5; return "we found crate full of ingredients (+5 veggies)"; },
      () => { G.inv.meat += 5; return "we found crate full of ingredients (+5 meat)"; },
      () => { G.inv.scrap += 6; return "we found some scrap (+6 scrap)"; },
      () => { G.inv.scrap += 5; return "we found some scrap (+5 scrap)"; },
      () => { G.inv.scrap += 4; return "we found some scrap (+4 scrap)"; },
      () => { G.inv.scrap += 3; return "we found some scrap (+3 scrap)"; },
      () => { G.inv.wool += 2; return "we found some fabric (+2 wool)"; },
      () => { G.inv.alcohol += 2; return "lucky find - we've found some alcohol (+2 alcohol)"; },
    ];
    return opts[Math.floor(Math.random() * opts.length)]();
  },
  (ch) => {
    var ammo0 = G.inv.ammo;
    var dmg = 0.1 + Math.floor(Math.random() * 0.1)
    var dmg2 = 0.2 + Math.floor(Math.random() * 0.4)

    // skillToughness decreases dmg
    if (ch.skillToughness) {
      dmg -= Math.random() * (ch.skillToughness / 40);
      if (dmg < 0.05) { dmg = 0.05 }

      // especially if no ammo
      dmg2 -= Math.random() * (ch.skillToughness / 10);
      if (dmg < 0.15) { dmg = 0.15 }
    }

    ch.hp -= dmg;
    ch.isDead = (ch.hp <= 0);

    if (G.inv.ammo <= 0) {
      ch.hp -= dmg2;
      ch.isDead = (ch.hp <= 0);
      if (ch.isDead) {
        return `ambush: ${ch.n} got hit dead; (we've got no ammo to defend ourselves!)`;
      }
      if (G.inv.firstAidKits > 0 && ch.hp < 0.5)
      {
        // skillHeal makes aid kits more effective
        G.inv.firstAidKits--;
        ch.hp = Math.min(1, ch.hp + 0.4 + (Math.random() * ch.skillHeal / 10));
        return `ambush: ${ch.n} got hit; (we've got no ammo to defand ourselves, firstAidKit--)`;
      }
      else
      {
        return `ambush: ${ch.n} got hit; (we've got no ammo to defend ourselves!)`;
      }
    }

    // skillShooting reduces ammo usage
    var dAmmo = (1 + Math.floor(Math.random() * (8 - (ch.skillShooting || 0))));
    if (dAmmo < 1) dAmmo = 1;

    G.inv.ammo = Math.max(0, G.inv.ammo - dAmmo);
    var ammo_diff = ammo0 - G.inv.ammo;
    if (ch.isDead) {
      return `ambush: ${ch.n} got hit dead; (-${ammo_diff} ammo)`
    }
    if (G.inv.firstAidKits > 0 && ch.hp < 0.5)
    {
      // skillHeal makes aid kits more effective
      G.inv.firstAidKits--;
      ch.hp = Math.min(1, ch.hp + 0.4 + (Math.random() * ch.skillHeal / 10));
      return `ambush: ${ch.n} got hit; (-${ammo_diff} ammo, -1 first-aid kit)`;
    }
    else
    {
      return `ambush: ${ch.n} got hit; (-${ammo_diff} ammo)`
    }
  }
];

const fail = (msg) => {
  G.fail = true;
  G.fail_msg = msg;
  refresh();
}

const explore = () => {
  // random alive leader
  var ls = G.chars.filter(ch => ch.leader && !ch.isDead);
  var ch = ls[Math.floor(Math.random() * ls.length)];

  var text = exploreEffects[Math.floor(Math.random() * exploreEffects.length)](ch)

  if (0 == G.chars.filter(ch => ch.leader && !ch.isDead).length) {
    fail("Your last leader died in the battle; the train cannot carry on without them.");
    return;
  }

  // todo: move to render?
  if(text.startsWith("ambush")) text = html`<big>&#9876;</big> ${text}`
  else text = html`<big>&#128230;</big> ${text}`;

  G.explorationInfo.push(text);
  refresh();
}

const exploreDone = () => {
  G.explorationFinished = true;

  // restore ap
  G.chars.filter(ch => !ch.isDead).forEach((ch) => {
    if (ch.leader) {
      ch.a = 1;
    } else {
      // ch.a = ch.hp < 0.5 ? 1 : 2;
      ch.a = 2;
    }
  });

  refresh();
}

const exploreDoneClick = () => {
  G.mode = "travel";
  G.travel++;
  refresh();
}

const startExplorationClicked = () => {
  G.mode = "exploration";
  G.explorationInfo = [];
  G.explorationFinished = false;

  refresh();

  // balance water:
  if (Math.random() < 0.75) {
    var w = Math.floor((Math.random() * 2) + 1);
    G.inv.watter += w;
    G.explorationInfo.push(html`<big>&#128230;</big> we found some drinkable watter (+${w} water)`);
  }

  const STEP = 220; // ms
  var i
  for (i=1; i<=5; i++) { window.setTimeout(explore, STEP* i); }
  i++;
  window.setTimeout(exploreDone, STEP* i + 100);
}

const endTravelClick = () => {
  var alives = G.chars.filter(ch => !ch.isDead);
  var actions_count = alives.map(ch => ch.a).reduce((a,b) => a+b, 0);

  var idle = actions_count > 0;
  var no_water = alives.length > G.inv.water;
  var no_food = alives.length > G.inv.food;

  if ((actions_count > 0) || no_water || no_food) {
    confirm(html`
      <p class="${idle ? "" : "hid"}">Some crewmember are still idle (they can still act)</p>
      <p class="${no_water ? "" : "hid"}">You don't have enough <b>water</b> for your crew (some of them might become discontent)</p>
      <p class="${no_food ? "" : "hid"}">You don't have enough <b>food</b> for your crew (some of them might become discontent)</p>
      <big>Continue?</big>
    `);
    G.confirm_ok = evaluateEndTravel;
    refresh();
    return;
  }

  // just do it!
  evaluateEndTravel()
  refresh();
}

const evaluateEndTravel = () => {
  G.modules.forEach(m => m.occupants = []);

  G.mode = "report";

  var f0 = G.inv.food;
  var w0 = G.inv.water;
  G.deserted_count = 0;

  var dead_starve = 0;
  var dead_thirst = 0;

  G.chars.filter(ch => !ch.isDead).forEach(ch => {
    if (ch.isDead) return;

    var wasUnhappy = ch.unhappy;

    if (G.inv.food > 0 && G.inv.water > 0) {
      // enough
      G.inv.food--;
      G.inv.water--;
      ch.unhappy = false;
    } else {
      // not enough
      G.inv.food--;
      G.inv.water--;

      if (ch.unhappy) {
        // Unhappy MAY desert
        if (Math.random() < (ch.leader ? 0.3 : 0.6)) {
          G.deserted_count++;
          ch.isDeserter = true;
          ch.isDead = true;
        }
      } else {
        ch.unhappy = true;
      }

      if (!ch.isDeserter){
        ch.hp -= G.inv.water < 0 ? 0.15 : 0;
        if (ch.hp < 0) {
          ch.isDead = true;
          dead_thirst++;
        }
        else {
          ch.hp -= G.inv.food < 0 ? 0.15 : 0;
          if (ch.hp < 0) {
            ch.isDead = true;
            dead_starve++;
          }
        }
      }
    }

    G.inv.food = Math.max(0, G.inv.food);
    G.inv.water = Math.max(0, G.inv.water);

    G.dead_starve = dead_starve;
    G.dead_thirst = dead_thirst;

    G.food_diff = f0 - G.inv.food;
    G.water_diff = w0 - G.inv.water;
  });

  if (G.chars.filter(ch => ch.leader && !ch.isDead).length == 0) {
    fail("All leader dead or deserted");
  }

  refresh();
}

const tryBuild = (idx) => {
  if (G.inv.scrap < G.toBeBuilt.cost) {
    alert("not enough scrap");

    G.toBeBuilt = false;
    G.build_menu_on = false;
    refresh();
    return;
  }

  if (G.modules[idx].isEmpty) {
    G.inv.scrap -= G.toBeBuilt.cost;
    G.modules[idx] = G.toBeBuilt.fn();

    G.toBeBuilt = false;
    G.build_menu_on = false;
    refresh();
    return;

  }

  G.toBeBuilt = false;
  G.build_menu_on = false;
  alert("Must be built on empty space");
  refresh();
}

const build_start = (what) => {
  if (G.inv.scrap < what.cost) {
    G.toBeBuilt = null;
    G.build_menu_on = false;
    alert("Too expensive");
    refresh();
    return;
  }

  G.toBeBuilt = what;
  G.build_menu_on = false;
  refresh();
}

const BuildMenuClicked = () => {
  if (G.toBeBuilt) {
    G.build_menu_on = false;
  } else {
    G.build_menu_on = !G.build_menu_on;
  }
  G.toBeBuilt = null;
  refresh();
}

const BuildMenu = (G) => {
  let copy = "Build"

  if (G.toBeBuilt) {
    copy = html`Build: <b>${G.toBeBuilt.name}</b> (cancel)`;
  }

  return html`
    <div
      @click="${() => { G.build_menu_on = false; refresh(); }}"
      class="build-menu-cover ${(G.build_menu_on) ? "" : "hid"}"
    >
    </div>
    <div class="build-menu ${Help.fc("build")}">
      <button @click="${BuildMenuClicked}">
        &#128296;
        ${copy}
      </button>

      <ul class="${(G.build_menu_on || Help.fc("build")) ? "" : "hid"}">
        ${G.to_build.map(it => html`
          <li
            @click="${() => build_start(it)}"
            class="build-li ${it.cost > G.inv.scrap ? "build-menu-expensive" : ""}"
          >
            ${it.icon || ""}
            ${it.name}
            <big>${it.cost}</big>
            <div class="build-li-tip">${it.tip}</div>
          </li>
        `)}
      </ul>
    </div>
  `;
}

const Root = (G) => {
  if (G.fail) {
    return html`
      <div style="padding: 20px; text-align: center;">
        <h1> You failed! </h1>
        <div style="font-size: 150px"> &#9760 </div>
        <br>
        <p>${G.fail_msg || ""}</p>
        <br>
        <button @click="${() => location.reload()}"> Try again </button>
      </div>
    `;
  }

  if (G.mode == "report") {

    const unhappy_count = G.chars.filter(c => !c.isDead && c.unhappy).reduce(a => a + 1, 0);

    return html`
      ${Inventory(G)}
      <div style="padding: 20px; text-align: center;" class="report">
        <strong style="display: block; margin: 20px;">Travel Report</strong>

        <p>your crew consumed ${G.food_diff} food and ${G.water_diff} water.</p>
        <p>${ G.deserted_count > 0 ? `${G.deserted_count} person(s) deserted.` : "" }</p>
        <p>${ unhappy_count > 0 ? html`&#x1F61E; ${unhappy_count} person(s) are discontent.` : "" }</p>

        <p>${ G.dead_starve > 0 ? `${G.dead_starve} person(s) starved to death.` : "" }</p>
        <p>${ G.dead_thirst > 0 ? `${G.dead_thirst} person(s) died of thirst.` : "" }</p>

        <button @click="${startExplorationClicked}">
          Scanvange nearby location
        </button>
      </div>
    `
  }

  if (G.mode == "travel") {
    var chars = G.chars.filter(c => !c.isDead);
    var chs = chars.length;
    var aps = chars.map(c => c.a).reduce((a, b) => a+b);

    return html`
      ${Inventory(G)}
      ${BuildMenu(G)}
      <strong>
        <div class="travel-num">
          Travel ${G.travel} &nbsp;
        </div>
        <button
          @click="${endTravelClick}"
          class="travel-b ${aps == 0 ? "travel-b-blink" : ""} ${Help.fc("done")}"
        >
          <span class="t-b">End Travel</span>
          <div class="travel-b-consumption">
            <span class="${chs > G.inv.food ? "travel-b-shortage" : ""}"> -${chs} food</span>,
            <span class="${chs > G.inv.water ? "travel-b-shortage" : ""}"> -${chs} water</span>
          </div>
        </button>
      </strong>
      ${Portraits(G.chars)}
      ${Modules()}

      <div class="alert ${G.alert ? "" : "hid"}">
        <div>
          ${G.alert}
          <br> <br> <br>
          <button autofocus @click="${() => { G.alert = ""; refresh(); }}"> OK </button>
        </div>
      </div>

      <div class="alert confirm ${G.confirm ? "" : "hid"}">
        <div>
          ${G.confirm}
          <br> <br> <br>
          <button autofocus @click="${() => { G.confirm = ""; G.confirm_ok(); refresh(); }}"> OK </button>
          <button autofocus @click="${() => { G.confirm_cancel(); }}"> Cancel </button>
        </div>
      </div>
    `;
  }

  if (G.mode == "exploration") {
    return html`
      ${Inventory(G)}
      <div class="exp">
        <strong>Exploration</strong>

        ${G.explorationInfo.map((line) => html`<div class="exp-line">${line}</di>`)}

        <button @click=${exploreDoneClick} class="exp-b ${G.explorationFinished ? "" : "hid"}">
          Back to train
        </button>
      </div>
    `;
  }
}

const _cheat = () => {
  G.cheats_used++;
  refresh();
}

const _removeAllWorkers = () => {
  G.chars.forEach((c) => {
    console.log(c.leader, c.isDead)
    if (!c.leader) { c.isDead = true; }
  });
  refresh();
}

const Cheats = () => {
  return html`
    <details class="cheats">
      <summary>Cheats ${G.cheats_used ? `(${G.cheats_used} used)` : ""}</summary>

      <button @click=${_=>{_removeAllWorkers(); }}>Remove All Workers</button>
      <button @click=${_=>{G.inv.food += 5; _cheat(); }}>+5 food</button>
      <button @click=${_=>{G.inv.water += 5; _cheat(); }}>+5 water</button>
      <button @click=${_=>{G.inv.scrap += 5; _cheat(); }}>+5 scrap</button>
      <button @click=${_=>{G.inv.veggies += 5; _cheat(); }}>+5 veggies</button>
      <button @click=${_=>{G.inv.meat += 5; _cheat(); }}>+5 meat</button>
      <button @click=${_=>{G.inv.wool += 5; _cheat(); }}>+5 wool</button>
      <button @click=${_=>{G.inv.ammo += 5; _cheat(); }}>+5 ammo</button>
      <button @click=${_=>{G.inv.firstAidKits += 5; _cheat(); }}>+5 first-aid kits</button>
      <button @click=${_=>{G.chars.forEach(c=>(c.hp = 1));_cheat()}}>heal all</button>
      <button @click=${_=>{G.chars.forEach(c=>(c.a=c.leader ?1:2));_cheat()}}>add action points</button>
    </details>
  `;
}


const Help = () => {
  var s = Help.steps[Help.stepIdx];
  var nextCopy = Help.steps[Help.stepIdx+1] ? "Next" : "Done";

  if (!s) return "";

  const y = `${s.xy[1] > 0 ? "top: " : "bottom: " }${Math.abs(s.xy[1])}px`;

  return html`
    <div class="help-bg"></div>
    <div class="help-tip" style="left: ${s.xy[0]}px; ${y}">
      ${s.text}

      <div class="help-buttons">
        <button
          @click="${() => { Help.stepIdx++; refresh(); }}"
        >
          ${nextCopy}
        </button>

        <button
          class="${Help.stepIdx == 0 ? "" : "hid" }"
          style="float: left;"
          @click="${() => { Help.stepIdx = 999; refresh(); }}"
        >
          Skip Tutorial
        </button>
      </div>
    </div>

    <div class="help-bottom">
      <button style="padding: 8px 16px;" @click="${() => { Help.stepIdx = 999; refresh(); }}">
        Skip Tutorial
      </button>
    </div>
  `;
}

Help.stepIdx = 0;

Help.fc = (k) => {
  var s = Help.steps[Help.stepIdx];

  return (s && (s.k == k)) ? " help-focus " : "";
}

Help.steps = [
{xy:[460,-270], k:"skip",
  text: html`Welcome to PT prototype. <br> Let's introduce you to the basics` },

{xy:[460,50], k:"res",
  text: `Those are your resources.` },

{xy:[10,50], k:"res-fw",
  text: html`
    <b>Food</b> and <b>water</b> are the most important. Each crewmember consumes 1 food and 1 water each travel
    <br><br>
    Hungry or thirsty crewmember become <b>discontent</b>.
    <br><br>
    Discontent crewmember might leave you!
  ` },

{xy:[195,50], k:"res-s",
  text: html`Scrap is used to build <b>modules</b> and <b>ammo</b>` },

{xy:[290,50], k:"res-vm",
  text: html`<b>Veggies</b> and <b>meat</b> are used to produce food.` },

{xy:[490,50], k:"res-w",
  text: html`<b>Wool</b> is one of the ingrediends of first-aid kits.` },

{xy:[585,50], k:"res-a",
  text: html`<b>Alcohol</b> is the other of the ingrediend of first-aid kits. But it can also be consumed by crew in a bar.` },

{xy:[685,50], k:"res-fight",
  text: html`<b>Ammo</b> and <b>First-aid</b> kits are used by your crew during scavanging.` },

{xy:[570,-40], k:"portraits",
  text: `This is your crew. Your task is to manage their taks, health and morale.` },

{xy:[570,-40], k:"portrait-l",
  text: html`3 crewmembers are <b>leaders</b> who work less in the train but are responible for scavanging and fighting enemies.` },

{xy:[570,-40], k:"portrait-w",
  text: html`Another 3 crewmembers are <b>workers</b>.` },

{xy:[570,-40], k:"p-ap",
  text: `As you can see, workers have 2 action points for each travel and leaders only 1` },

{xy:[570,-10], k:"p-hover",
  text: html`Each crewmember has their own <b>skill</b>. Hover over their potrait to learn more about it.` },

{xy:[570,-10], k:"p-hover",
  text: html`Your crew will gain experience (and skill level) while working in related modules or in <b>Archive</b>` },

{xy:[490,-20], k:"modules",
  text: html`Here you can see your modules or rooms.` },

{xy:[490,-20], k:"mod-cl",
  text: html`You can send your crew to <b>Cluttered Space</b> to clean it up and gain some <b>scrap</b>. A cleaned up Cluttered Space becomes an <b>Empty Space</b>` },

{xy:[490,-40], k:"mod-e",
  text: html`<b>Empty Space</b> is where you can build modules. Building them cost some <b>scrap</b>` },

{xy:[200,100], k:"build",
  text: html`You can build modules by selecting them from the Build Menu and placing them on an empty space.
  <br>
  <br>
  Hover over modules to display more information about them.
  ` },

{xy:[885,140], k:"done",
  text: html`When you ready you can end travel. Your <b>leaders</b> will scavenge nearby location.` },
];

const RootRoot = () => {
  return html`
    <div id="rootroot">
      ${Root(G)}
      ${Cheats()}
      ${Help()}
    </div>
  `
}

const refresh = () => render(RootRoot(G), document.getElementById("app"))
refresh()

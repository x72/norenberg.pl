let recalc = function() {};

const $ = document.querySelector.bind(document)
const $$ = (sel) => [...document.querySelectorAll(sel)];

const getVal = (id) => +$("#" + id).value;

const leaders_conf = {
  l_ethan:  { key: "l_ethan",  hpMax: 100, power: .15, ammo_factor: 1 }, // better heal
  l_anna:   { key: "l_anna",   hpMax:  90, power: .15, ammo_factor: 0.75 }, // uses less ammo
  l_rene:   { key: "l_rene",   hpMax: 100, power: .15, ammo_factor: 1 }, // infection
  l_mevlit: { key: "l_mevlit", hpMax: 150, power: .20, ammo_factor: 1.3 }, // uses more ammo
  l_esihle: { key: "l_esihle", hpMax: 110, power: .20, ammo_factor: 1 }, // might use scrap
  l_olena:  { key: "l_olena",  hpMax: 130, power: .25, ammo_factor: 0.5 }, // doesn't use ammo
  l_zhang:  { key: "l_zhang",  hpMax: 130, power: .25, ammo_factor: 0.5 }, // doesn't use ammo
}

const items_conf = {
  i_vaccine: { regular: true}, // infection fix

  i_firstaid: { heal: 0.25, regular: true }, // heal
  i_smelling_salts: { heal: 0.5, regular: true }, // heal item

  i_snag:           { dmg: .08, regular: true },
  i_molotov:        { dmg: .07, regular: true },
  i_infection_bomb: { dmg: .06, regular: true },
  i_decoy:          { dmg: .06, regular: true },

  i_armor: { dmg_fac: 0.7 },
  i_hermes: { dmg: 0.05 },
  i_piercing_bullets: { dmg: 0.10 },
  i_ariadne: { dmg: 0.05 },
  i_explosive_bullets: { dmg: 0.15 },

  i_face_mask: {}, // infection prevention
  i_lockpicks: {}, // make crowbar redundant
};

const leaders_keys = Object.keys(leaders_conf);

leaders_keys.forEach(k => $("#" + k).onchange = () => { recalc() });

const getLeaders = () => {
  return leaders_keys
    .map(k => $("#" + k))
    .filter(it => it.checked)
    .map(it => leaders_conf[it.id]);
}

const isPresent = (key) => getLeaders().filter(l => l.key == key).length > 0;


const newRange = (info, id, min, max, step, init) => {


  const clone = document.querySelector('#range_template').content.cloneNode(true);
  clone.querySelector("span").innerText = info;
  const input = clone.querySelector("input");

  const i = clone.querySelector("i");
  input.id = id;
  input.min = min;
  input.max = max;
  input.step = step;

  if (init || init == 0) input.value = init;

  const e_update = () => {
    recalc();
    if (min == 0 && max == 1 && step < 0.1) {
      i.innerText = (+input.value).toFixed(2);
    }
    else {
      i.innerText = input.value;
    }
  }
  input.onchange = e_update;
  input.oninput = e_update;
  e_update();

  return clone;
}

{
  $("#ranges_wrap").appendChild(newRange("Distance from HQ", "distance", 1, 16, 1, 4));
  $("#ranges_wrap").appendChild(newRange("Ammo", "ammo", 0, 300, 1, 30));
  $("#ranges_wrap").appendChild(newRange("Init Infection", "init_infection", 0, 1, 0.01, 0));

  $("#ranges_hp_wrap").appendChild(newRange("leader_a hp (n)", "l_a_hp", .01, 1, .01, .5));
  $("#ranges_hp_wrap").appendChild(newRange("leader_b hp (n)", "l_b_hp", .01, 1, .01, .5));

  const r2 = $("#ranges2_wrap");
  r2.append(newRange("Danger Modifier (bosses?)", "e_danger_level", -40, 50, 1, -15));
  r2.append(newRange("Ammo per hp (CONF important)", "e_ammo_per_hp_conf", .01, .25, .01, .05));
  r2.append(newRange("Enemies total HP", "e_total_hp", 500, 2500, 10));
  r2.append(newRange("Infection fogs (contaminated zone => + 10 fogs)", "e_infections", 0, 10, 1, 0));
}

const Backpack = [];

{
  const newItemSelect = (id, tpl = "#items_template", bg=null, max=16) => {
    const clone = document.querySelector(tpl).content.cloneNode(true);
    clone.querySelector("select").id = id;
    clone.querySelector("select").style.background = bg;

    clone.querySelector("select").oninput = () => recalc();


    const range = newRange("#", id+"_range", 1, 16, 1, 1)
    range.querySelector("label").style.display = "";
    clone.querySelector("div").append(range);


    return clone;
  }

  const put_a = (a) => $("#items_wrap_1").appendChild(a);
  const put_b = (a) => $("#items_wrap_2").appendChild(a);
  put_a(newItemSelect("item_1"));
  put_a(newItemSelect("item_2"));
  put_a(newItemSelect("item_3"));
  put_a(newItemSelect("item_4", "#special_items_template", "#fe4", 1));

  //$("#items_wrap").append(document.createElement("br"));

  put_b(newItemSelect("item_5"));
  put_b(newItemSelect("item_6"));
  put_b(newItemSelect("item_7"));
  put_b(newItemSelect("item_8", "#special_items_template", "#fe4", 1));

  const $item_types = $$(`select[id^="item_"]`);
  const $item_counts = $$(`input[id^="item_"]`);

  $item_types.forEach((select, idx) => {

    const input = $item_counts[idx];

    Backpack.push({
      id: select.id, select, input,
      getInfo: () => ({
        key: select.value,
        conf: items_conf[select.value] || {},
        count: +input.value
      }),
    });
  })

  Backpack.getCount = (itemType) => {
    return Backpack
      .filter(it => it.select.value == itemType)
      .map(it => +it.input.value)
      .reduce((a, b) => a + b, 0)
  }

  Backpack.getCountIfEquipped = (leader_index, itemType) => {
    const li = (leader_index == 0 ? [1,2,3,4] : [5,6,7,8]).map(n => "item_" + n)

    return Backpack
      .filter(it => it.select.value == itemType)
      .filter(it => li.includes(it.id))
      .map(it => +it.input.value)
      .reduce((a, b) => a + b, 0)
  }

}

function l_pow(id) {
  return ($(id).checked) ? leaders_conf[id].power : 0;
}

function getItemCount(itemType)
{

  // $item_types
  //   .filter(())
}

const isOnlyZhangAndOrOlena = () => {
  const ls = getLeaders();
  if (ls.length == 1) return isPresent("l_zhang") || isPresent("l_olena");
  if (ls.length == 2) return isPresent("l_zhang") && isPresent("l_olena");
}

const $item_types = $$(`select[id^="item_"]`);
const $item_counts = $$(`input[id^="item_"]`);

const getCurrentBiom = () => {
  const dist = getVal("distance");
  if (dist <= 5) return 1;
  if (dist <= 10) return 2;
  return 3;
}

const willLeaderDie = (idx, dmg) => {
  const ls = getLeaders();
  if (!ls[idx]) return false; // already dead

  const hp = ls[idx].hpMax * getVal(idx == 0 ? "l_a_hp" : "l_b_hp")
  return hp < dmg;
}

const setBg = (biom) => {
  document.body.style.background = [
    "white",
    "#faeac6",
    "#c0e4c0",
    "#d3eaef",
  ][biom];

  $("h1").innerText = [
    "🚂",
    "🌞",
    "🌴",
    " ☃️  ",
  ][biom];
}

// ok it's a mess but this function basically:
recalc = function() {
  const items_used = {};
  const ls = getLeaders();
  const show_main_params = (ls.length == 1 || ls.length == 2);
  $("#main_params").style.display = show_main_params ? "block" : "none";

  if (!show_main_params) { setBg(0); return };
  setBg(getCurrentBiom());

  // Base power on of the components of POWER
  // it is sum of all the power of each leader present (and alive)
  // + some constant: `e_danger_level`
  // TBD: the constant is per level or per dificulty or trurely constant?
  const l_base_pow =
    + ls.reduce((mem, l) => l.power + mem, 0)
    - (getVal("e_danger_level") / 100)
  ;

  // Item pow is another component of POWER
  // it uses all fight-items asigned in the backpack:
  // following regular items affect POWER:
  //   i_snag, i_molotov, i_infection_bomb, i_decoy
  // and special:
  //   i_armor, i_hermes, i_piercing_bullets, i_ariadne, i_explosive_bullets
  let l_item_pow = 0;

  // first leader bonuses - for each damaging item it's dmg is added
  Backpack
    .slice(0, 4)
    .map(it => it.getInfo())
    .filter(it => it.conf.dmg)
    .forEach(it => {
      l_item_pow += it.conf.dmg
    })

  // second leader bonuses - note the check so two leaders cannot share the bonus for
  // the same item
  Backpack
    .slice(4, 8)
    .map(it => it.getInfo())
    .filter(it => it.conf.dmg)
    .forEach(it => {
      const count = Backpack.getCount(it.key);
      const other_leader_has_it = Backpack.getCountIfEquipped(0, it.key) > 0;

      if (count <= 1 && other_leader_has_it){
        // pass
      } else {
        l_item_pow += it.conf.dmg
      }
    })

  // Abundance modifier - another POWER component
  // up to doubles dmg by each damaging item based on it's quantity in INVENOTRY
  // ignores who have it the Backpack as long as it's equipped
  // each item over 1 adds 0.1 of base dmg bonus
  let l_item_abundance_bonus = 0;
  Object.entries(items_conf).forEach(([i_key, conf]) => {
    if (conf.dmg) {
      const count_over_1 = Backpack.getCount(i_key) - 1;
      if (count_over_1 > 0) {
        l_item_abundance_bonus += Math.min(conf.dmg, count_over_1 / 10 * conf.dmg);
      }
    }
  })

  // Damage factor reduces enemies damage before they even hit the leader
  // up to 4 armors can potentially be equipped so:
  // 0.8 ** 0 = 1
  // 0.8 ** 1 = 0.8
  // 0.8 ** 2 = 0.64
  // 0.8 ** 3 = 0.512
  // 0.8 ** 4 = 0.41
  const l_dmg_armor_fac = 0.8 ** ($item_types.filter(it => it.value == "i_armor").length)


  // ammo needed calculations:
  const enemies_hp_sum = getVal("e_total_hp");

  // The e_ammo_per_hp_conf is used for balance
  // in game code it could be an actual constant or a literal
  // ammo_needed is total of the needed ammo 
  let ammo_needed = enemies_hp_sum * getVal("e_ammo_per_hp_conf");

  // product of all leaders ammo_factor
  const ammo_fac_prod = ls.reduce((mem, l) => l.ammo_factor * mem, 1);

  // If there is only Zhang or only Olena or only they two than we dont really use ammo
  // those characters deal higher flat damage then other leaders
  if (isOnlyZhangAndOrOlena()) {
    ammo_needed = 0;
  } else {
    // Zhang or Olena (or Anna btw) still reduce the amount of ammo used
    // Mevlit need more ammo (for better flat dmg and hp)
    ammo_needed *= ammo_fac_prod;
  }
  ammo_needed = Math.floor(ammo_needed)

  // l_ammo_bonus_pow is another POWER component
  // If we have less ammo then ammo needed each missing ammo reduces POWER by 1%
  const ammo_missing = Math.max(0, ammo_needed - getVal("ammo"));

  // if we have too much ammo leaders don't save as much so they deal more damage
  // up to 30% this way
  // doesn't work with Zhang and Olena - moreover they also reduce the effectivenes 
  // of oversuply of ammo for other leaders
  let ammo_overflow = Math.min(30, Math.max(0, getVal("ammo") - ammo_needed));
  if (isOnlyZhangAndOrOlena()) ammo_overflow = 0;
  const zhang_olena_mod = (isPresent("l_zhang") || isPresent("l_olena")) ? 0.5 : 1;
  ammo_overflow *= zhang_olena_mod;

  // finally the POWER component
  const l_ammo_bonus_pow = 
    ammo_missing * -0.01
    + ammo_overflow * 0.01
  ;


  // this is base final ammo
  const almost_final_ammo = (Math.max(0, getVal("ammo") - ammo_needed))
  // up this amount of ammo will be used (random amount) additionally to the required amount
  const ammo_overflow_half = Math.floor(ammo_overflow / 2);

  // infection
  // infection is applied for each fog element on the map
  // on the contaminated maps we should set this as if there are many (12?) fogs
  let final_infection = 0;
  {
    // inf_per_fog flatly adds some infection per each fog out there
    let inf_per_fog = 0.05; // this value migh need to be balanced

    // If we have squad composed entirely of Rene and/or a masked (special item) leaders then
    // fogs deal no infection to us
    // If we have only half of the party who is Rene and/or masked then the fog
    // infection rate is halfed
    if (ls.length == 1) {
      if (isPresent("l_rene") || Backpack.getCount("i_face_mask")) {
        inf_per_fog = 0;
      }
    } else if(ls.length == 2) {
      if (ls[0].key == "l_rene" || Backpack.getCountIfEquipped(0, "i_face_mask"))
        inf_per_fog -= 0.025;

      if (ls[1].key == "l_rene" || Backpack.getCountIfEquipped(1, "i_face_mask"))
        inf_per_fog -= 0.025;
    }

    const init_infection = getVal("init_infection");
    const fogs_count = getVal("e_infections");

    let cur_infection = init_infection + (inf_per_fog * fogs_count)

    // the leaders ALWAYS try to reduce the infection over 0.5 with vaccines
    while (cur_infection > 0.5 && Backpack.getCount("i_vaccine") > 0) {
      cur_infection -= 0.3333;

      // we store information on number of vaccines used so it can be presented to the player
      // after the autoresolve
      items_used["i_vaccine"] = (items_used["i_vaccine"] || 0) + 1
    }

    final_infection = cur_infection;
  }


  // POWER is the number we show for the player (probably as success 
  // chance: "low", "medium", "high") or % -- TBD
  const POWER = (l_base_pow + l_item_pow + l_item_abundance_bonus + l_ammo_bonus_pow)
  const sum = Math.floor(POWER * 100);
  // const _dmg = (100-sum) < 0 ? 0 : Math.floor(100 - sum);


  // final damage to be delt to both players this can be still will be mitigated with healing
  let damage = (1-POWER) * 100;
  damage *= l_dmg_armor_fac; // armors

  // easier in the beginging
  const dist = getVal("distance");
  if (dist == 1) damage *= 0.50;
  if (dist == 2) damage *= 0.70;
  if (dist == 3) damage *= 0.90;

  // per biom modificator (to be balanced)
  const b = getCurrentBiom();
  if (b == 1) damage *= 0.9;
  if (b == 2) damage *= 1.0;
  if (b == 3) damage *= 1.1;


  // heal
  const init_a_hp = getVal("l_a_hp") * ls[0].hpMax;
  const init_b_hp = ls[1] ? (getVal("l_b_hp") * ls[1].hpMax) : 0;

  let final_a_heal = 0;
  let final_b_heal = 0;

  let final_a_hp = init_a_hp - damage;
  let final_b_hp = init_b_hp - damage;
  {
    while(true) {
      // no first_aid kits -> break
      if ((Backpack.getCount("i_firstaid") - (items_used["i_firstaid"] || 0)) <= 0) {
        break;
      }

      let hp_heal_amount = 0.15; // todo: base on skills and items?

      if (isPresent("l_ethan")) { hp_heal_amount *= 2; }

      // two steps
      // leaders try to heal themselves if they have less then 30hp
      {
        const hpMax = ls[0].hpMax;
        const hp = init_a_hp - damage + (hpMax * final_a_heal);

        if (hp <= 30) {
          final_a_heal += hp_heal_amount;
          final_a_hp = init_a_hp - damage + final_a_heal * hpMax;
          items_used["i_firstaid"] = (items_used["i_firstaid"] || 0) + 1; // mark item used
        }
      }

      // no first_aid kits -> break
      if ((Backpack.getCount("i_firstaid") - (items_used["i_firstaid"] || 0)) <= 0) {
        break;
      }

      if (ls[1]) {
        const hpMax = ls[1].hpMax;
        const hp = init_b_hp - damage + (hpMax * final_b_heal);

        if (hp <= 30) {
          final_b_heal += hp_heal_amount;
          final_b_hp = init_b_hp - damage + final_b_heal * hpMax;
          items_used["i_firstaid"] = (items_used["i_firstaid"] || 0) + 1; // mark item used
        }
      }

      // only one leader or both of them have at list a bit of hp - no need to heal
      if (final_a_hp > 10 && (ls.length == 1 || final_b_hp > 10)) {
        break;
      }
    }
  }

  // salts are only used if firstaidkits wasnt enough
  if (final_a_hp <= 0 && final_b_hp <= 0) {
    // both are dead

    if (Backpack.getCount("i_smelling_salts") >= 2) {
      // enough salts for both

      final_a_hp = 1;
      final_b_hp = 1;

      items_used["i_smelling_salts"] = 2;

    } else if (Backpack.getCount("i_smelling_salts") == 1) {
      // choosing one to survive (the other dies)
      if (final_a_hp < final_b_hp) {
        final_a_hp = 1;
      } else {
        final_b_hp = 1;
      }

      items_used["i_smelling_salts"] = 1;

    } else {
      // tough luck - both die!
    }

  } else if(final_a_hp <= 0) {
    if (Backpack.getCount("i_smelling_salts") >= 1) {
      final_a_hp = 1;
      items_used["i_smelling_salts"] = 1;
    }

  } else if(final_b_hp <= 0) {
    if (Backpack.getCount("i_smelling_salts") >= 1) {
      final_b_hp = 1;
      items_used["i_smelling_salts"] = 1;
    }
  }

  // Display:

  const printLeaderHpInfo = ($el, l, init_hp, final_hp) => {
    if (!l) { $el.innerText = ""; return; }

    $el.style.color = final_hp <= 0 ? "red" : "";

    $el.innerText = [
      `${l.key} hp: ${init_hp.toFixed(2)} → ${final_hp.toFixed(2)}`,
      `${final_hp > 0 ? "" : " 💀 "}`,
    ].join(" ");
  }

  printLeaderHpInfo($("#precog_final_heal_a"), ls[0], init_a_hp, final_a_hp);
  printLeaderHpInfo($("#precog_final_heal_b"), ls[1], init_b_hp, final_b_hp);

  $("#precog_final_power").innerText = "power: " + sum + "%";
  $("#precog_final_infection").innerText = "final infection: " + final_infection.toFixed(2);

  $("#precog_final_hp_lost").innerText =
    `dmg per leader: ${Math.max(5, damage).toFixed(2)}`;

  $("#precog_ammo").innerText = `final ammo: ${almost_final_ammo} - random (0, ${ammo_overflow_half})`;
  //---

  const $small = $("#precog_small")
  $small.innerText = [
    [
      `POWERS:: base: ${l_base_pow.toFixed(2)}`,
      `item: ${l_item_pow.toFixed(2)}`,
      `abundance: ${l_item_abundance_bonus.toFixed(2)}`,
      `ammo: ${l_ammo_bonus_pow.toFixed(2)}`,
    ].join(", "),
    isPresent("l_esihle") ? "[!] Esihle -- 40% chance of using 10 scrap" : "",
    Backpack.getCount("i_ariadne") ? "[!] Ariadne's Thred: if anyone dies 50% chance of ending up with 1hp insteaed" : "",
    Backpack.getCount("i_lockpicks") ? "[!] Lockpicks : don't consume corwbars" : "",
    l_item_abundance_bonus > 0 ? "[!] abundance items have 20% chance of being used" : "",
    `item used (chance to use up more): \n` +
    Object.entries(items_used).map(([k, c]) => `  + ${k}: ${c}\n`).join("") +
    `  + all regular damaging items (snag, molotov, infection bomb, decoy)`

  ].filter(Boolean).map(l => "- " + l).join("\n");
}


recalc();

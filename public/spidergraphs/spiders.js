const render = (len) => {
  const values1 = Array(len).fill(1).map(() => Math.max(0.1, Math.random()));
  const values2 = values1.map((v) => Math.max(v, Math.min(1, v + Math.random() * 0.6 - 0.1)));

  const labels = [
    "Project Management...",
    "IT Project Management",
    "Computer Techincal Support",
    "Customer Service Management",
    "Business/Comerce General",
    "Business Operations support and...",
    "Thinking outside the box",
    "Negotiations",
    "Conflict resolution",
    "Conflict management",
    "Evaluating Job Candidates",
    "Achieving Goals",
    "Assessing Progress Towards Departmental Goals",
    "Conflict Management",
    "Creating Budgets for Business Units",
    "Creating Financial Reports",
    "Conflict Resolution",
    "Decision Making",
    "Delegation",
    "Delivering Presentations",
    "Division of Work",
    "Empowerment",
    "Engagement",
    "Evaluating Job Candidates",
    "Evaluating Employee Performance",
    "Execution",
    "Focus, Goal Orientation",
    "Goal Setting",
    "Hiring",
    "Interacting with Individuals from Diverse Backgrounds",
    "Interpersonal",
    "Interpreting Financial Data",
    "Interviewing Candidates for Jobs",
    "Leadership",
    "Motivation",
    "Overcoming Obstacles",
    "Productivity",
    "Problem Solving",
    "Professionalism",
    "Providing Constructive Criticism",
    "Recommending Cost-Cutting Measures",
    "Recommending Process Improvements",
    "Responding Favorably to Criticism",
    "Responsibility",
    "Training Employees",
    "Verbal Communication",
  ]
    .sort(() => Math.random() < 0.5)
    .slice(0, len);

  const tau = Math.PI * 2;
  const MAX = 90; // this many units is 1.0
  const LINE_MAX = 92;

  const getXY = (val, angle) => {
    // negative y so we start drawing clockwise from 12 o'clock
    return `${val * MAX * Math.sin(angle)} ${val * MAX * -Math.cos(angle)}`;
  }

  const xy = (val, i) => {
    const angle = i / values1.length * tau;
    return [val * MAX * Math.sin(angle), val * MAX * -Math.cos(angle)]
  };

  const points1 = values1.map(xy);
  const points2 = values2.map(xy);

  const svgPath = (points, fn, values) =>
    points.reduce((acc, p, index) => index === 0 ? `M ${p[0]},${p[1]}` : `${acc} ${fn(p, index, points, values)}`, "");

  const lineFn = p => `L ${p[0]} ${p[1]}`

  // all points start at 0 0 so it's simpler:
  const getLength = (point) => (point[0]**2 + point[1]**2)**0.5;

  const bezierFn = (point, i, points, values) => {  // start control point

    // -1 because the push
    const len = points.length - 1;
    const angle = (i / len) * Math.PI * 2 + Math.PI / 2;

    const prevI = points[i - 1] ? i-1 : points.length - 1;
    const prevPoint = points[prevI];
    const prevAngle = (prevI / len) * Math.PI * 2 - Math.PI / 2;

    let size = 10;
    // when adjecent points of small value the bezier blob gets tangles, so let's make it less bloby
    // console.log(getLength(prevPoint), getLength(point), ":::", values[prevI], values[i]);
    if (values[prevI] < 0.25 && values[i] < 0.25) {
      size = 5;
      // console.log("REDUCED SIZE", prevI, i);
    }
    // console.log(".", values[prevI], values[i]);

    const xx2 = point[0] + Math.sin(angle) * -size;
    const yy2 = point[1] - Math.cos(angle) * -size;

    const xx1 = prevPoint[0] + Math.sin(prevAngle) * -size;
    const yy1 = prevPoint[1] - Math.cos(prevAngle) * -size;

    return `C ${xx1},${yy1} ${xx2},${yy2} ${point[0]},${point[1]}`
  }
  const defs = `
  <defs>
    <radialGradient id="RadialGradientBlue">
      <stop offset="0%" stop-color="#51a6fe"/>
      <stop offset="100%" stop-color="#2459f8"/>
    </radialGradient>
  </defs>
  `;

  const lines = `
  <g stroke="rgba(0,0,0,0.1)" stroke-width="0.5" fill="transparent">
    <circle cx="0" cy="0" r="25" data-lines="c" />
    <circle cx="0" cy="0" r="50" data-lines="c" />
    <circle cx="0" cy="0" r="75" data-lines="c" />
    ${
      values1.map((val, i) => {
        const angle = i / values1.length * tau;
        return `<line x1="0" y1="0" x2="${LINE_MAX * Math.sin(angle)}" y2="${LINE_MAX * -Math.cos(angle)}" data-lines="${i+1}" />\n`;
      }).join("\n")
    }
  </g>
  `;



  const blob1 = `
    <path
      data-graph="1"
      fill="url(#RadialGradientBlue)"
      d="${svgPath(points1.concat([points1[0]]), bezierFn, values1)}"
    />
  `;

  const blob2 = `
    <path
      data-graph="2"
      fill="#eef755"
      d="${svgPath(points2.concat([points2[0]]), bezierFn, values2)}"
    />
  `;

  const blobs = [blob2, blob1];

  const $svgWrap = document.querySelector(".svgWrap");
  $svgWrap.innerText = "";
  $svgWrap.innerHTML = `
    <div class="labels"></div>
    <svg viewBox="-120 -120 240 240" style="width: 600px; height: 600px;"></svg>
  `;

  const $svg = document.querySelector("svg");
  $svg.innerText = "";
  $svg.innerHTML = `
    ${defs}
    ${blobs.join("\n")}
    ${lines}
  `
  window.setTimeout(() => $svg.classList.add("on"), 10);

  const $labels = document.querySelector(".labels");
  $labels.innerText = "";

  labels.forEach((label, i) => {
    const turn = i / labels.length;
      const innerTurn = 0.25 < turn && turn < 0.75 ? 0.5 : 0;
    $labels.innerHTML += `
      <div style="transform: rotate(${turn}turn)">
        <span style="transform: rotate(${innerTurn}turn)">
          ${label}
        </span>
      </div>
    `;
  });
};

render(6);

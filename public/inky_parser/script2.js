const $log = document.querySelector("#log-short");

var input = document.getElementById("i")
var acsv = document.getElementById("csv")
var ajson = document.getElementById("json")

const NO_CACHE = {}

var hash_cache = {}
const hash = (str, category) => {
  str = str.replace("<b>", "").replace("</b>", "");

  var h = Array.from(str)
    .reduce((hash_val, char) => 0 | (31 * hash_val + char.charCodeAt(0)), 0)
    .toString(36)

  // if (category == NO_CACHE) return h;

  // category will lower chance for duplication
  var ckey = category + ":" + h;
  if (!hash_cache[ckey]){ hash_cache[ckey] = 0 }
  hash_cache[ckey]++;

  return h + "_" + hash_cache[ckey];
}

var zJSON = (str) => {
  // console.log(str)
  return 'data:application/json;charset=utf-8, ' + encodeURIComponent(str);
}

var zCSV = (str) => {
  // console.log(str)
  return 'data:plain/text;charset=utf-8, ' + encodeURIComponent(str);
}


const reAbout1 = /\.\.\. ?o [A-Z][a-z]*\?$/;
const reAbout2 = /\.\.\. ?about [A-Z][a-z]*\?$/;

const oneNonrepeats = [
  "O kim?", "About who?", "About whom?",
  "OK",
  "Yes ma'am!",
  "??",
]

const reRoom1 = /Room schematics/;
const reRoom2 = /^ ?<sprite name=[a-z_"]*>\) ?$/;

var k2save = (k, text) => {
  if (text.match(reAbout1) || text.match(reAbout2)) {
    return "@.x." + hash(text.split(" ").at(-1));
  }

  if (oneNonrepeats.includes(text)) {
    return "@.x." + hash(text);
  }

  if (text.match(reRoom1) || text.match(reRoom2)) {
    return "@.x." + hash(text);
  }

  var v1 = k.replace(/^_\.2/, "@");

  var dotIdx = v1.indexOf(".", 2);
  if (dotIdx > 0) {
    v1 = v1.slice(0, dotIdx);
  }

  if (k.match("hq_Esihle")) 
  {
    debugger
  }

  return v1 +"#"+ hash(v1 + ":" + text, v1);
}

var ALL_KEYS_UNOPTED_TO_LINES = {};

var k2saveCSV = k2save;
var k2saveJSON = (k, t) => {
  var key = k2save(k, t);

  if (ALL_KEYS_UNOPTED_TO_LINES[key] && ALL_KEYS_UNOPTED_TO_LINES[key].choice) {
    key = key.replace("@.", "@opt.");
  }

  return ("%%" + key + "%%");
}

var INK = null;
var gjson = null;

input.addEventListener("change", (e) => {
  new Response(input.files[0]).json().then(json => {
    INK = JSON.parse(JSON.stringify(json)); // for debuging

    let json1 = JSON.parse(JSON.stringify(json))
    let list1 = start(json1, k2saveCSV)
    let csv = list1.map((it) => {
      ALL_KEYS_UNOPTED_TO_LINES[it.k] = it.text;
      var k = it.choice ? it.k.replace("@.", "@opt.") : it.k;
      return `${k}\t${it.text}`
    }).join("\n")
    acsv.setAttribute('href', zCSV(csv));

    console.log("CSV", csv);

    gjson = json1


    // var json2 = JSON.parse(JSON.stringify(json))
    // var list2 = start(json2, k2saveJSON)
    // var outjson = JSON.stringify(json2)
    // ajson.setAttribute('href', zJSON(outjson));

    // gjson = json2

    // console.log("JSON", outjson);

  }, err => {
      alert("error parsing json file");
  })
}, false);


var FIRST_PREFIX = "_.2.REPORTS"
var saved = []

var rePlan = /^Plan[A-Z][a-z][^ ]*$/
var skips = ["_.2.global decl"]
var shouldSkip = (knot, text) => {
  for (var i=0; i<skips.length; i++) {
    if (knot.match(skips[i])) { return true; }
  }

  if(text.replace("^", "").trim() === "") return true;

  if(text === "...") return true; // continuation (won't be translated)
  if(text.match("fn:::") || text.match("fn :::")) return true; // debug ink functions
  if(knot.match("BoneCollector") && text.match(rePlan)){
    return true; // don't translate plans enums
  }

  if (knot.startsWith("_.2.HUB")) return true; // redundant?

  return false;
}

var started = false
const start = (json, k2s) => {
  $log.innerText = "...";
  hash_cache = {};
  saved = [];

  started = false;
  parse("_", json.root, k2s);

  return saved;
}

var last_translatable_line = null;
var CHOICES = [];

const parse = (knot, item, k2s) => {
  if (typeof item == "string" && item[0] == "^") {
    // text
    if (knot.match("START_PARSING")) {
      started = true;
    }

    var text = item.replace("^", "").trim()

    // if (knot.match("hq_M")) { debugger; }

    if (text && started && !shouldSkip(knot, text)) {
      if (text == "^") return false;

      var key = k2s(knot, text)
      var l = { k: key, text }
      last_translatable_line = l;

      // this sucks but what can I do..
      var isCSV = (k2s == k2saveCSV)
      if (isCSV && key.startsWith("@.x.") && !key.endsWith("_1")){
        return false;
      }

      saved.push(l)
      return true
    }

    return false
  }

  else if (Array.isArray(item)) {
    // container
    item.forEach((it, i) => {
      var k = knot + "." + i;
      if (parse(k, it, k2s)) {
        item[i] = "^" + k2s(k, "^"); // ??
      }
    })
  }

  else if (typeof item == "object") {

    if ((item || {})["*"] && last_translatable_line) {
      // previously added line is a choice

      last_translatable_line.choice = true;
      CHOICES.push(last_translatable_line.text);

    } else {
      // each key is a subcontainer
      Object.entries(item || {}).forEach(([k, varr]) => {
        parse(knot + "." + k, varr, k2s)
      });
    }

  }
}

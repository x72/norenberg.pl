const zJSON = (str) => 'data:application/json;charset=utf-8, ' + encodeURIComponent(str);
const zCSV = (str) => 'data:plain/text;charset=utf-8, ' + encodeURIComponent(str);
const do_hash = (str) => {
  str = str
    .replace("<b>", "").replace("</b>", "")
    .replace("<desc>", "").replace("</desc>", "")
    .replace("<i>", "").replace("</i>", "");

  return Array.from(str)
    .reduce((hash_val, char) => 0 | (31 * hash_val + char.charCodeAt(0)), 0)
    .toString(36)
    .replace(/^-/, "0");
}

const commons = {
  "Yes ma'am!": "yes_maam",
  "About who?": "about_who",
  "About whom?": "about_whom",
  "OK": "ok",
  "Thanks for the talk.": "thanks4talk",
  "What do you think about...": "what_think_about",
  "You look like something is bothering you...": "bothered",
  "I would like to ask about someone else. What can you tell me about...": "about_sb_else",
}
const smartHash = (knot, text) => {
  if (commons[text]) {
    return "@.x." + commons[text];
  }
  if (text.match(/^\.\.\.about [A-Z][a-z]+\?$/)) {
    // ...about Anna?, ...about Ethan? etc.
    return "@.x." + text.toLowerCase().replace(" ", "_").replace(/[^a-z]/g, "");
  }

  return knot + "#" + do_hash(`${knot}:${text}`) + "@";
}

var collisions_count = 0;
var INK = null;
var ROOT_CONTAINER = [];
var DATA_FOR_CSV = {}
var OPT_HASHES = {}
var CSV = "";

function get_sep()
{
  return document.cookie
    .split(";")
    .filter(it => it.startsWith("_sep="))
    ?.[0]
    ?.split("=")
    ?.[1] || "tab";
}

(document.querySelector(`[name="sep"][value="${get_sep()}"]`) || {}).checked = true;
document.querySelectorAll('[name="sep"]').forEach(it => {
  it.onchange = () => {
    document.cookie = "_sep=" + it.value;
  }
});


function SEP()
{
  let _sep = get_sep();
  if (_sep == "semicolon") return ";";
  if (_sep == "dblsemicolon") return ";;";
  if (_sep == "tabl") return "\t";
  return "\t";
}

var input = document.getElementById("i");
input.addEventListener("change", (e) => {
  if (e.target.files[0].name.endsWith(".json") == false) {
    alert("pass .json not an .ink file!")
    return;
  }

  new Response(input.files[0]).json().then(json => {
    INK = JSON.parse(JSON.stringify(json));

    let json1 = JSON.parse(JSON.stringify(json))
    traverse(ROOT_CONTAINER, "@", json1.root)

    // export
    var csvText = "";
    Object.entries(DATA_FOR_CSV).forEach(([hash, eng]) => {
      // todo: opt prefix
      if (get_sep() == "onlykey") {
        csvText += hash + "\n";
      } else {
        csvText += hash + SEP() + eng + "\n";
      }
    })

    var jsonText = JSON.stringify(json1);

    // opt hashes
    Object.keys(OPT_HASHES).forEach(opthash => {
      var opthash_prefixed = opthash.replace(/^@\./, "@opt.");
      csvText = csvText.replaceAll(opthash, opthash_prefixed);
      jsonText = jsonText.replaceAll(opthash, opthash_prefixed);
    });

    jsonText = jsonText.replaceAll("@.", "^@.").replaceAll("@opt", "^@opt"); // ^ is required in json file

    document.getElementById("json").setAttribute('href', zJSON(jsonText));
    document.getElementById("csv").setAttribute('href', zCSV(csvText));
    console.log(jsonText);
    console.log(csvText);
    document.querySelector("body").className = "";
    document.querySelector("#ta_json").value = jsonText;
    document.querySelector("#ta_csv").value = csvText;

    if (collisions_count > 0) { alert("COLLISIONS: " + collisions_count); }

  }, err => {
    alert("error parsing json file");
    throw err;
  })
}, false);

const knotStripEnd = (knot) => {
  var idx = knot.indexOf(".", 2);
  return idx > 0 ? knot.slice(0, idx) : knot;
}

var last_hash = null;
var active = false;
const traverse = (container, knot, item) => {
  if (typeof item == "string" && item[0] == "^") {
    last_hash = null;
    if (knot.match("START_PARSING")) active = true;
    if (knot.match("global decl")) return;
    var text = item.slice(1);

    if (text== "...") return;
    if (text.match("fn:::")) return; // functions in some places
    if (text== "??") return;
    if (text.trim() == "") return;
    if (text.match(/^Plan[A-Z][a-zA-Z]+$/)) return; // Bone collector fix

    if (active && !knot.match("START_PARSING"))
    {
      var knot2 = knotStripEnd(knot);
      const hash = smartHash(knot2, text);

      // update JSON
      for (var i=0; i<container.length;i++) {
        if (container[i] == item) {
          container[i] = hash;
        }
      }

      last_hash = hash;

      // data for CSV
      if (DATA_FOR_CSV[hash] && (DATA_FOR_CSV[hash] !== text)) {
        collisions_count++;
        console.error(`COLLISION: ${knot}: ${text}`)
      }
      DATA_FOR_CSV[hash] = text;
    }
  }
  else if (Array.isArray(item)) {
    // container
    // update val in container? -- should I pass container to traverse?
    item.forEach((subitem) => traverse(item, knot, subitem))
  }
  else if (typeof item == "object") {
    if ((item || {})["*"]) {
      // previously added line is a choice
      if (last_hash) {
        OPT_HASHES[last_hash] = true;
      }
      last_hash = null;

    } else {
      // each key is a subcontainer
      Object.entries(item || {}).forEach(([subknot, subitem]) => {
        traverse(container, knot + "." + subknot, subitem);
      });
    }
  }
}


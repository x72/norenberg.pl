import { jsPDF } from "./jspdf.es.min.js";

// Default export is a4 paper, portrait, using millimeters for units
//


const genOnePDF = (p) => {
  console.log("genOnePDF", p);

  const isOn = (prop) => {
    try {
      return document.getElementById("ch_" + prop).checked;
    } catch(_) {
      return false;
    }
  }

  const doc = new jsPDF();

  let y = 20;
  const line = (txt) => {
    doc.text(txt, 10, y);
    y += 10;
  }

  // NAME
  doc.setFontSize(32);
  line(p.first_name + " " + p.last_name);

  doc.setFontSize(16);

  // EMAIL
  isOn("email") && line("EMAIL: " + p.email);

  // TITLE
  if (isOn("title")) {
    let title = ""
    try {
      title = p.positions.find((p) => p.main).job_title[0].value
    } catch(_) {}
    title && line("TITLE: " + title);
  }

  // STATUS
  isOn("status") && line("STATUS: " + p.status);

  // SKILLS
  if (isOn("skills")) {
    line("SKILLS:")
    p.skills.forEach((s) => {
      line("    " + s.value);
    });
  }

  // office
  isOn("office") && line("OFFICE: " + p.office[0].value);

  // --- saving ---

  //doc.save(p.first_name + "_" + p.last_name +"_a4.pdf");
  return doc.output("arraybuffer");
}


window.doExport = () => {
  const p1 = genOnePDF(JSON.parse(document.querySelector("#profile_1").value));
  const p2 = genOnePDF(JSON.parse(document.querySelector("#profile_2").value));

  const t0 = performance.now()

  var zip = new JSZip();
  zip.file("01.pdf", p1);
  zip.file("02.pdf", p2);

  zip.generateAsync({ type:"blob" })
    .then((content) => saveAs(content, "allofthem.zip"));

  document.getElementById("perf").innerText =
    "GENERATED 2 PROFILES IN: " + (performance.now() - t0) + "ms";
};

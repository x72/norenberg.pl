function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

let current_word = null;
const total_len = WORDS.length;
const WORDS_CLONE = WORDS.slice();
const ws = WORDS;
shuffle(ws);


const $ = document.querySelector.bind(document)

function newEl(type, text, props)
{
  const e = document.createElement(type);
  for (let k in props || {}) {
    e.setAttribute(k, props[k]);
  }
  e.innerText = text || "";
  return e;
}


$("#wordssetname").innerText = WORDS_SET_NAME;
for (let k in WORDSETS) {
  $("#setbuttons").append(newEl("a", k, { href: `?set=${k}` }));
}

function doneAll() {
  showInfo();
}

function getOneWordInfo(w) {
  const state = localStorage.getItem(w.front) || "";
  const ones = state.replace(/[^1]/g, "").length;
  const zeros = state.replace(/[^0]/g, "").length;

  const normalized = (ones / (ones + zeros)) || 0;

  const strike = state.endsWith("1".repeat(10));

  return {
    ones: ones,
    zeros: zeros,
    sum: ones + zeros,
    normalized: normalized,
    strike: strike,
    text: `${ones} / ${ones + zeros} (${parseInt(normalized * 100 + 0.5)}%)`,
  };
}


function showInfo() {
  $("#all").style.display = "none";
  $("#total").style.display = "none";
  $("#info").style.display = "block";

  const ul = $("#info > ul")
  ul.innerText = "";
  WORDS_CLONE.forEach((w) => { w.score = getOneWordInfo(w); });
  WORDS_CLONE.sort((a, b) => a.score.normalized > b.score.normalized);

  const statsof = $("#statsof")
  statsof.innerText = WORDS_SET_NAME;


  WORDS_CLONE.forEach((w) => {
    const li = newEl("li")
    const div = newEl("div", w.front);
    div.append(newEl("i", w.back));
    li.append(div);
    const span = newEl("span", w.score.text);
    li.append(span);
    if (w.score.strike) {
      li.style.color = "#4a3";
    }
    if (w.score.sum === 0) {
      span.style.opacity = 0.3;
    }
    ul.append(li);
  });

}

function showNext() {
  const word = ws.pop()
  if (!word) { return doneAll(); }

  current_word = word
  $("#front").innerText = word.front
  $("#back").innerText = word.back;
  $("#back").style.visibility = "hidden";
  $("#buttons").style.visibility = "hidden";
  $("#total").innerText = (ws.length + 1) + " / " + total_len;
}

function record(key, success)
{
  const state = localStorage.getItem(key) || "";
  const newState = state + "" + (success ? 1 : 0);
  localStorage.setItem(key, newState);
}

$("#ok").onclick = (e) => {
  e.stopPropagation();
  record(current_word.front, true);
  showNext();
  return false;
}

$("#fail").onclick = (e) => {
  e.stopPropagation();
  record(current_word.front, false);
  ws.push(current_word);
  shuffle(ws);
  showNext();
  return false;
}

$("#center").onclick = () => {
  $("#back").style.visibility = "visible";
  $("#buttons").style.visibility = "visible";
}

$("#total").onclick = showInfo;

$("#clear_info").onclick = () => {
  for (var key in localStorage){
    localStorage.removeItem(key)
  }
  showInfo();
}

showNext();

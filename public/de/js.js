const g = {};

const $in = document.getElementById("in");
const $curr = document.getElementById("curr");
const $score = document.getElementById("score");
const $inWrapInfo = document.querySelector(".inputWrapInfo");
const $report = document.getElementById("report");
const $sets = document.getElementById("sets");

$sets.addEventListener("change", (e) => {
  fetch(e.target.value)
    .then(resp => resp.json())
    .then(data => start(data))
    .catch(() => alert("un error :("));
});

const _letter2accent = { a:"ä", o:"ö", u:"ü" };
$in.addEventListener("keyup", (e) =>{
  // accents
  console.log(e.key)
  if((e.key === ";" || e.key === ":") && $in.selectionStart === $in.selectionEnd) {
    const index = $in.selectionStart - 2;
    var prev = $in.value[index];
    if (_letter2accent[prev]) {
      $in.value = $in.value.slice(0, index) + _letter2accent[prev] + $in.value.slice(index + 2);
      $in.selectionStart = $in.selectionEnd = index + 1;
    }
  } else if (e.key === "S" || e.key === "Shift") {
    const index = $in.selectionStart - 1;
    var prev = $in.value[index];
    $in.value = $in.value.slice(0, index) + "ß" + $in.value.slice(index + 1);
    $in.selectionStart = $in.selectionEnd = index + 1;
  }

  if (!g.ready) return;

  // answer
  if (e.keyCode === 13) {
    const val = e.target.value.trim();

    if (val === "!!!") {
      done();
    } else if (val === "R") {
      start(g.initialSet); // restart
    } else if (val === "X") {
      restartWithErrorsOnly();
    } else if (val === "?") {
      if (!$in.placeholder) {
        $in.value = "";
        $in.placeholder = g.targets.join("; ");
        g.helps++;
        g.reportHelps.push(g.entries[g.curr][0]); // store english
      }
    } else {
      let notCorrect = true;
      const matchVal = val.replace(/[\.\!\?]/g, ""); //only for phrases
      for (let i=0; i < g.targets.length; i++) {
        const t = g.targets[i];
        if (
          val === t
          || val === t.replace(/^las? /, "")
          || val === t.replace(/^el /, "")
          || val === t.replace(/^los /, "")
          || matchVal === t.replace(/[\.\!\?]/g, "")
        )
        {
          correct();
          notCorrect = false;
          break;
        }
      }
      if(notCorrect) {
        triggerError();
      }
    }
  }
});

const triggerError = () => {
  $in.setAttribute("data-error", true);
  window.setTimeout(() => $in.removeAttribute("data-error"), 300);

  if (!g.stepError) {
    g.reportErrors.push(g.entries[g.curr][0]); // store english
    g.stepError = true;
  }
}

const restartWithErrorsOnly = () => {
  if (!g.done) return;
  if (g.reportErrors.length + g.reportHelps.length === 0) {
    ghostinfo("X \\o/");
    return;
  }

  const setOfErrors = {}; // yeah, "list" is not a list - TODO: fix, nor is the initia

  for(k in g.initialSet){
    if(g.reportErrors.indexOf(k) > -1 || g.reportHelps.indexOf(k) > -1) {
      setOfErrors[k] = g.initialSet[k];
    }
  }

  start(setOfErrors);
  $sets.value = "";
};

const start = (list) => {
  const entries = Object.entries(list);

  g.done = false;
  g.initialSet = list;

  g.reportErrors = [];
  g.reportHelps = [];

  g.helps = 0;
  g.ready = true;
  g.entries = entries.sort(() => 0.5 - Math.random()); // good enough
  g.curr = 0;
  g.total = entries.length;

  $in.value = "";
  $in.placeholder = "";
  $report.innerText = "";

  step();
}

const ghostinfo = (text) => {
  $inWrapInfo.innerText = text;
  $inWrapInfo.removeAttribute("data-show");
  window.clearTimeout($inWrapInfo._timeout);

  window.setTimeout(() => {
    $inWrapInfo.setAttribute("data-show", true);
    $inWrapInfo._timeout = window.setTimeout(() => {
      $inWrapInfo.removeAttribute("data-show");
    }, 2000);
  }, 10);
};

const correct = () => {
  g.entries.splice(g.curr, 1);
  g.curr = (Math.random() * g.entries.length)|0;

  $in.setAttribute("data-correct", true);
  window.setTimeout(() => $in.removeAttribute("data-correct"), 500);

  ghostinfo(g.targets.join("; "));

  step();
};

const step = () => {
  $in.placeholder = "";
  $in.value = "";
  g.stepError = false; // one error per entry

  if (g.entries.length === 0) {
    done();
  } else {
    g.targets = g.entries[g.curr][1].split("|");
    $curr.innerText = g.entries[g.curr][0];
    $score.innerText = (g.total - g.entries.length) + "/" + g.total;
  }

  $in.focus();
}

const done = () => {
  if (g.entries.length > 0) {
    g.reportErrors = g.reportErrors.concat(g.entries.map((pair) => pair[0])); // pluck english
    g.entries = [];
  }

  g.done = true;

  let good = 0;

  $report.innerText = "";
  Object
    .entries(g.initialSet)
    .map((entry) => ({
      err: g.reportErrors.indexOf(entry[0]) >= 0,
      help: g.reportHelps.indexOf(entry[0]) >= 0,
      text: `${entry[1].split("|").join("; ")} - ${entry[0]}`
    }))
    .sort((a, b) => ((b.err || b.help) ? 1 : 0) - ((a.err || a.help) ? 1 : 0))
    .map((it) => {
      const li = document.createElement("li");
      li.innerText = it.text;
      if (it.err || it.help) { li.classList.add("isErr"); }
      else { good++ };

      if (it.help) {
        const small = document.createElement("small");
        small.innerText = "(ayuda)";
        li.append(small);
      }
      return li;
    })
    .forEach((li) => $report.append(li));

  $curr.innerText = good + "/" + g.total + " ("+(((good/g.total)*100)|0)+"%)"
  $score.innerText = g.helps === 0 ? "sin ayudas" : `con ${g.helps} ${g.helps === 1 ? "ayuda" : "ayudas"}`;
}


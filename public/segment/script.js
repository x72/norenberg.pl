const size = 512;
const canvas = document.getElementById("c");
canvas.width = size;
canvas.height = size;
const ctx = canvas.getContext("2d");

const img = new Image();
img.src = "mri.jpg"
img.onload = () => {
  ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, size, size);
};

document.getElementById("run-btn").onclick = () => {
  run();
}

const isUniform = (imgData) => {
}

const quad = (tree, x, y, w, h) => {
  if (w < 4 || h < 4) return;

  // node
  const node = newNode(tree, x, y, w, h);
  node.sum = 0

  // node props
  const imgData = ctx.getImageData(x, y, w, h);
  let max = -Infinity;
  let min = Infinity;
  for (let i = 0; i < imgData.data.length; i += 4) {
    const r = imgData.data[i]; // 0 .. 255
    node.sum += r;
    if (r > max) max = r;
    if (r < min) min = r;
  }

  node.average = Math.floor((node.sum / (imgData.data.length / 4)) + 0.5);

  const diff = max - min;
  node.isUniform = diff < parseInt(document.getElementById("diff_input").value);

  // children
  if (!node.isUniform) {
    quad(node, x,     y,     w/2, h/2);
    quad(node, x+w/2, y,     w/2, h/2);
    quad(node, x,     y+h/2, w/2, h/2);
    quad(node, x+w/2, y+h/2, w/2, h/2);
  }
}


const run = () => {
  ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, size, size);

  const tree = [];
  gTree = tree; // global
  quad(tree, 0, 0, size, size);

  ctx.beginPath();
  //drawNodesAverage(tree);
  drawNodesOutline(tree);
  ctx.stroke();
}

const newNode = (parent, x, y, w, h) => {
  const n = [];
  n.x = x; n.y = y;
  n.w = w; n.h = h;
  parent.push(n)
  return n;
}

const drawNodesOutline = (tree) => {
  ctx.fillStyle = "transparent";
  ctx.strokeStyle = "red";
  ctx.rect(tree.x, tree.y, tree.w, tree.h);
  tree.forEach(drawNodesOutline);
}

const drawNodesAverage = (tree) => {
  if (tree.average != undefined)
  {
    const a = tree.average;
    ctx.fillStyle = `rgb(${a}, ${a}, ${a})`;
    ctx.strokeStyle = "transparent";
    ctx.fillRect(tree.x, tree.y, tree.w, tree.h);
  }
  tree.forEach(drawNodesAverage);
}

